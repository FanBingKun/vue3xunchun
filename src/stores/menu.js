import { defineStore } from 'pinia'

// useStore 可以是 useUser、useCart 之类的任何东西
// 第一个参数是应用程序中 store 的唯一 id
export default defineStore('menu', {
  // other options...
  state: () => {
    return {
      getMenuFlag: false,
      menuList: []
    }
  },
  actions: {
    setMenuList(data) {
      this.menuList = data
      this.getMenuFlag = true
    }
  }
})