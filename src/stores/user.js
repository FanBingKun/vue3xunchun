
import { defineStore } from "pinia"
import { reactive} from 'vue'
export const useAppStore = defineStore(
  "user",
  () => {
    // 状态
    const state = reactive({
      userInfo: {}
    })
    return { state }
  }
)
