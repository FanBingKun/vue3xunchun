import { createRouter, createWebHistory } from "vue-router"
import Layout from "@/layout/index.vue"


const constantRoutes = [
  {
    path: "/",
    component: Layout,
    redirect: "/dashboard",
    children: [
      {
        path: "dashboard",
        name: "Dashboard",
        component: () =>
          import(
            /* webpackChunkName: "dashboard" */ "@/views/dashboard/index.vue"
          ),
      }
    ]
  },
  {
    path: "/test",
    component: Layout,
    children: [
      {
        path: "",
        name: "test",
        component: () => import("@/views/test.vue"),
      },
      {
        path: "/test/:id",
        component: () => import("@/views/detail.vue"),
      }
    ]
  },
  {
    path: "/test2",
    component: Layout,
    children: [
      {
        path: "kk",
        name: "test2",
        component: () => import("@/views/test2.vue"),
      },
    ]
  },
  {
    path: "/404",
    component: () => import("@/views/error-page/404.vue"),
    meta: {
      hidden: true // 404 hidden掉
    }
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("@/views/login/index.vue")
  },
]
export const routes = constantRoutes // 默认只绑定常量路由

export default createRouter({
  history: createWebHistory(),
  routes
})
