import { createApp } from 'vue'
import { createPinia } from 'pinia'
import "normalize.css/normalize.css"
import "@/styles/index.less"
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'



import App from './App.vue'
import router from './router'


import "./permission"

const app = createApp(App)
app.use(ElementPlus)
app.use(createPinia())
app.use(router)
// Vue.prototype.http = () => {
// }
app.config.globalProperties.$json = (obj) => {
  return JSON.parse(JSON.stringify(obj))
}
app.mount('#app')
